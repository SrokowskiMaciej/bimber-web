import {ChatType, ChatVisibleChatMembership, Group, Message, Paths} from "./model";
import {children} from "./utils";
import * as admin from "firebase-admin";
type DataSnapshot = admin.database.DataSnapshot;
const database: admin.database.Database = admin.database();


export function getChatInfo(chatId: string, actingUserId: string): Promise<ChatInfo> {
    return getActiveChatMemberships(chatId).then(chatMemberships => getChatType(chatMemberships)
        .then(chatType => {
            let activeMembersIds = chatMemberships.map(membership => membership.uId);
            if (chatType === "DIALOG") {
                return getDialogInfo(chatId, actingUserId, activeMembersIds)
            } else if (chatType === "GROUP") {
                return getGroupInfo(chatId, actingUserId, activeMembersIds)
            } else {
                return Promise.reject("No such chat type")
            }
        }))
}


export function getActiveChatMemberships(chatId: string): Promise<ChatVisibleChatMembership[]> {
    return database.ref(Paths.MEMBERSHIPS_CHAT_AGGREGATED)
        .child(chatId)
        .orderByChild("membershipStatus")
        .equalTo("ACTIVE")
        .once("value")
        .then((dataSnapshot: DataSnapshot) => {
            if (dataSnapshot.hasChildren()) {
                let value = dataSnapshot.val();
                let chatMembersIds: ChatVisibleChatMembership[] = Object.keys(value).map(key => value[key]);
                console.log("Active memberships: ", chatMembersIds);
                return chatMembersIds;
            } else {
                console.log("No active chat memberships");
                return [];
            }
        });
}

export function getGroupInfo(chatId: string, actingUserId: string, activeMembersIds: string[]): Promise<ChatInfo> {
    return getGroup(chatId)
        .then(group => {
            if (group.image) {
                return Promise.resolve(new ChatInfo(chatId, group.name, "GROUP", [new PhotoInfo(chatId, group.image)],
                    activeMembersIds, group.groupOwnerId))
            } else {
                //Select uIds that will be a part of collage image. We should start with sending uId but not repeat it
                let selectedUsersIds = Array.from(new Set([actingUserId].concat(activeMembersIds))).slice(0, 3);
                return Promise.all(selectedUsersIds.map(uId => getUserPhoto(uId).then(value => new PhotoInfo(uId, value))))
                    .then(photos => new ChatInfo(chatId, group.name, "GROUP", photos, activeMembersIds, group.groupOwnerId))
            }
        })
}

export function getDialogInfo(chatId: string, actingUserId: string, activeMembersIds: string[]): Promise<ChatInfo> {
    return Promise.all([getUserName(actingUserId), getUserPhoto(actingUserId)])
        .then(values => new ChatInfo(chatId, values[0], "DIALOG", [new PhotoInfo(actingUserId, values[1])], activeMembersIds, null))
}

export function getGroup(chatId: string): Promise<Group> {
    return database.ref(Paths.GROUPS)
        .child(chatId)
        .once("value")
        .then((snapshot: DataSnapshot) => snapshot.val())
}

export function getUserName(uId: string): Promise<string> {
    return database.ref("users")
        .child(uId)
        .child("displayName")
        .once("value")
        .then((snapshot: DataSnapshot) => snapshot.val())
        .catch(() => null)
}

export function getChatType(chatMemberships: ChatVisibleChatMembership[]): Promise<ChatType> {
    if (chatMemberships.length > 0) {
        return Promise.resolve(chatMemberships[0].chatType)
    } else {
        return Promise.reject("No memberships")
    }
}

export function getUserPhoto(uId: string): Promise<string> {
    return database.ref("photos")
        .child(uId)
        .orderByChild("index")
        .limitToFirst(1)
        .once("value")
        .then((snapshot: DataSnapshot) => children(snapshot))
        .then(values => {
            if (values.length > 0) {
                //Add photo type
                let value: any = values[0];
                return value.uri
            }
            else
                return null
        })
        .catch(() => null)

}

export class PhotoInfo {
    constructor(readonly sourceId: string,
                readonly photoUri: string) {
    }
}

export class MessageInfo {
    constructor(readonly userName: string,
                readonly messageId: string,
                readonly messageTimestamp: number) {
    }
}

export class ChatInfo {
    constructor(readonly chatId: string,
                readonly chatName: string,
                readonly chatType: ChatType,
                readonly photos: PhotoInfo[],
                readonly activeChatMembersIds: string[],
                //Should be null in dialogs
                readonly ownerId: string) {
    }
}