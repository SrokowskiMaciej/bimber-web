import * as functions from 'firebase-functions';
import * as admin from "firebase-admin";
import {getProjectCredentialsFilePath, getProjectId} from "./credentialsProvider";

const THUMB_PREFIX = 'thumb_';
const BLUR_PREFIX = 'blur_';
const THUMB_MAX_DIMENSION = 100;

let request = require('request-promise-native');
let gm = require('gm').subClass({imageMagick: true});
let path = require('path');
let os = require('os');
let fs = require('fs-extra');
let download = require('image-downloader')
const Storage = require('@google-cloud/storage');
const vision = require('@google-cloud/vision')();
const storage = new Storage({
    projectId: getProjectId(),
    keyFilename: getProjectCredentialsFilePath(),
});

type PhotoType = "USER" | "GROUP"

export let onStoragePhotoUploaded = functions.storage.object().onFinalize(((object, context) => {
    // File and directory paths.
    const filePath = object.name;
    const bucketName = object.bucket;

    const fileDir = path.dirname(filePath);
    const fileName = path.basename(filePath);

    const photoId = path.basename(filePath).replace(THUMB_PREFIX, "").replace(BLUR_PREFIX, "");
    const sourceId = path.basename(fileDir);

    console.log('File path: ' + filePath + ", file name: " + fileName + ", sourceId: " + sourceId + ", photoId: " + photoId);
    // Exit if the image is already a thumbnail.

    let photoType: PhotoType
    if (filePath.startsWith("photos/profiles")) {
        photoType = "USER";
    } else if (filePath.startsWith("photos/groups")) {
        photoType = "GROUP";
    } else {
        return Promise.resolve('Not public image');
    }

    // Exit if this is triggered on a file that is not an image.
    if (!object.contentType.startsWith('image/')) {
        return Promise.resolve('This is not an image.');
    }

    // Exit if the image is already a thumbnail.
    if (fileName.startsWith(THUMB_PREFIX)) {
        return Promise.resolve('Already a Thumbnail.');
    }

    const blurredFileName = `${BLUR_PREFIX}${fileName}`;
    const thumbFileName = `${THUMB_PREFIX}${fileName}`;

    const uploadBlurredFilePath = path.normalize(path.join(fileDir, blurredFileName))
    const uploadThumbnailFilePath = path.normalize(path.join(fileDir, thumbFileName))

    const tempLocalFile = path.join(os.tmpdir(), filePath) + ".jpg";
    const tempLocalBlurredFile = path.normalize(path.join(os.tmpdir(), fileDir, blurredFileName)) + ".jpg";
    const tempLocalThumbFile = path.normalize(path.join(os.tmpdir(), fileDir, thumbFileName)) + ".jpg";

    return isPublicSafe(bucketName, filePath)
        .then(safe => {
            if (safe) {
                return createThumbnail(bucketName, filePath, tempLocalFile, tempLocalThumbFile)
                    .then(() => {
                        if (photoType == "USER") {
                            return uploadProfileThumbnail(bucketName, uploadThumbnailFilePath, sourceId, photoId, tempLocalThumbFile)
                        } else if (photoType == "GROUP") {
                            return uploadGroupThumbnail(bucketName, uploadThumbnailFilePath, sourceId, photoId, tempLocalThumbFile)
                        } else {
                            return Promise.reject("NO SUCH TYPE")
                        }
                    })
            } else {
                return blurImage(bucketName, filePath, tempLocalFile, tempLocalBlurredFile)
                    .then(() => {
                        if (photoType == "USER") {
                            return uploadProfileBlurredFile(bucketName, uploadBlurredFilePath, sourceId, photoId, tempLocalBlurredFile)
                                .catch(err => {
                                    let censoredValues = {
                                        uri: "",
                                        censored: true
                                    };
                                    return admin.database().ref("photos").child(sourceId).child(photoId).update(censoredValues)
                                        .then(() => storage.bucket(bucketName).file(filePath).delete())
                                        .then(() => Promise.reject("Failed to blur image, deleted profile uri, cause: " + err.toString()))
                                });
                        } else if (photoType == "GROUP") {
                            return uploadGroupBlurredFile(bucketName, uploadBlurredFilePath, sourceId, photoId, tempLocalBlurredFile)
                                .catch(err => {
                                    return admin.database().ref("groups").child(sourceId).child("image").set(null)
                                        .then(() => storage.bucket(bucketName).file(filePath).delete())
                                        .then(() => Promise.reject("Failed to blur image, deleted group image, cause: " + err.toString()))
                                });
                        } else {
                            return Promise.reject("NO SUCH TYPE")
                        }
                    })

            }
        })
        .then(() => {
            unlinkSyncSafe(tempLocalFile);
            unlinkSyncSafe(tempLocalBlurredFile);
            unlinkSyncSafe(tempLocalThumbFile);
            return Promise.resolve();
        })
        .catch(err => {
            unlinkSyncSafe(tempLocalFile);
            unlinkSyncSafe(tempLocalBlurredFile);
            unlinkSyncSafe(tempLocalThumbFile);
            return Promise.reject(err)
        });
}));

function isPublicSafe(bucketName: string, filePath: string): Promise<boolean> {
    const request = {
        source: {
            imageUri: `gs://${bucketName}/${filePath}`
        }
    };
    return vision.safeSearchDetection(request).then(data => {
        const detections = data[0].safeSearchAnnotation;
        console.log('SafeSearch results on image', JSON.stringify(data));
        return !(detections.adult === 'LIKELY' ||
            detections.adult === 'VERY_LIKELY' ||
            detections.violence === 'VERY_LIKELY')
    });
}

function blurImage(bucketName: string, filePath: string, tempLocalFile: string, tempLocalBlurredFile: string): Promise<void> {
    const file = storage.bucket(bucketName).file(filePath);

    return fs.ensureDir(path.dirname(tempLocalFile)).then(() => {
        // Download file from bucket.
        return file.download({destination: tempLocalFile});
    }).then(() => {
        return new Promise(((resolve, reject) => {
            gm(tempLocalFile)
                .blur(0, 60)
                .write(tempLocalBlurredFile, function (err) {
                    if (!err) {
                        console.log("Created blurred file");
                        resolve("Success")
                    } else {
                        console.log("Not blurred file: " + err);
                        reject(err)
                    }
                })
        }))
    })
}

function createThumbnail(bucketName: string, filePath: string, tempLocalFile: string, tempLocalThumbFile: string): Promise<void> {
    const file = storage.bucket(bucketName).file(filePath);
    return fs.ensureDir(path.dirname(tempLocalFile)).then(() => {
        // Download file from bucket.
        return file.download({destination: tempLocalFile});
    }).then(() => {
        console.log('The file has been downloaded to', tempLocalFile);
        // Generate a thumbnail using ImageMagick.
        return new Promise(((resolve, reject) => {
            gm(tempLocalFile)
                .thumb(THUMB_MAX_DIMENSION, THUMB_MAX_DIMENSION, tempLocalThumbFile, function (err) {
                    if (!err) {
                        console.log("Created thumbnail");
                        resolve("Success")
                    } else {
                        console.log("Not created thumbnail: " + err);
                        reject(err)
                    }
                })
        }))
    })

}

function uploadProfileThumbnail(bucketName: string, uploadFilePath: string, profileId: string, photoId: string,
                                localThumbFilePath: string): Promise<void> {

    const bucket = storage.bucket(bucketName);
    const file = bucket.file(uploadFilePath);
    return bucket.upload(localThumbFilePath, {destination: uploadFilePath})
        .then(() => {
            console.log('Thumbnail uploaded to Storage at', uploadFilePath);
            // Get the Signed URLs for the thumbnail and original image.
            const config = {
                action: 'read',
                expires: '03-01-2500'
            };
            return file.getSignedUrl(config);
        }).then(results => {
            console.log('Got Signed URLs.');
            const thumbFileUrl = results[0];
            // Add the URLs to the Database
            return admin.database().ref("photos").child(profileId).child(photoId).child("thumb").set(thumbFileUrl);
        }).then(() => console.log('Thumbnail URLs saved to database.'));
}

function uploadGroupThumbnail(bucketName: string, uploadFilePath: string, groupId: string, photoId: string,
                              localThumbFilePath: string): Promise<void> {

    const bucket = storage.bucket(bucketName);
    const file = bucket.file(uploadFilePath);
    return bucket.upload(localThumbFilePath, {destination: uploadFilePath})
        .then(() => {
            console.log('Thumbnail uploaded to Storage at', uploadFilePath);
            // Get the Signed URLs for the thumbnail and original image.
            const config = {
                action: 'read',
                expires: '03-01-2500'
            };
            return file.getSignedUrl(config);
        }).then(results => {
            console.log('Got Signed URLs.');
            const thumbFileUrl = results[0];
            // Add the URLs to the Database
            return admin.database().ref("groups").child(groupId).child("thumb").set(thumbFileUrl);
        }).then(() => console.log('Thumbnail URLs saved to database.'));
}

function uploadProfileBlurredFile(bucketName: string, uploadFilePath: string, profileId: string, photoId: string,
                                  localBlurredFilePath: string): Promise<void> {
    const bucket = storage.bucket(bucketName);
    const file = bucket.file(uploadFilePath);

    return bucket.upload(localBlurredFilePath, {destination: uploadFilePath})
        .then(() => {
            console.log('Blurred file uploaded to Storage at', uploadFilePath);
            // Get the Signed URLs for the thumbnail and original image.
            const config = {
                action: 'read',
                expires: '03-01-2500'
            };
            return file.getSignedUrl(config);
        }).then(results => {
            console.log('Got Signed URLs.');
            const bluredFileUrl = results[0];
            // Add the URLs to the Database
            let censoredValues = {
                uri: bluredFileUrl,
                censored: true
            };
            return admin.database().ref("photos").child(profileId).child(photoId).update(censoredValues);
        })
}

function uploadGroupBlurredFile(bucketName: string, uploadFilePath: string, groupId: string, photoId: string,
                                localBlurredFilePath: string): Promise<void> {
    const bucket = storage.bucket(bucketName);
    const file = bucket.file(uploadFilePath);

    return bucket.upload(localBlurredFilePath, {destination: uploadFilePath})
        .then(() => {
            console.log('Blurred file uploaded to Storage at', uploadFilePath);
            // Get the Signed URLs for the thumbnail and original image.
            const config = {
                action: 'read',
                expires: '03-01-2500'
            };
            return file.getSignedUrl(config);
        }).then(results => {
            console.log('Got Signed URLs.');
            const bluredFileUrl = results[0];
            // Add the URLs to the Database
            return admin.database().ref("groups").child(groupId).child("image").set(bluredFileUrl);
        })
}

function unlinkSyncSafe(file: string) {
    if (fs.existsSync(file)) {
        fs.unlinkSync(file)
    }
}