/**
 * Created by maciek on 05.04.17.
 */
import * as admin from "firebase-admin";
const functions = require('firebase-functions');

import {GroupMeetingNotificationType, Key, Group, Paths} from './model'
import {sendPushMessages} from './pushMessaging'
import {now} from "./utils";
import {ChatInfo, getChatInfo, PhotoInfo} from "./notificationData";

const GeoFire = require('geofire');
type DataSnapshot = admin.database.DataSnapshot;
const database: admin.database.Database = admin.database();

export class TimeConstants {
    public static readonly MINUTE = 60 * 1000;
    public static readonly HOUR = 60 * TimeConstants.MINUTE;

    public static readonly PULL_BOUNDARY = -2 * TimeConstants.MINUTE;

    public static readonly GROUP_ACTIVATION_TIME = -24 * TimeConstants.HOUR;
    public static readonly GROUP_MEETING_REMINDER_TIME = -2 * TimeConstants.HOUR;
    public static readonly GROUP_MEETING_TIME = 0;
    public static readonly GROUP_DEACTIVATION_TIME = 6 * TimeConstants.HOUR;
}

export let subscribeToPeriodicGroupTimeCheckTick = functions.pubsub.topic('group-time-check-tick')
    .onPublish(() => now().then((now: number) => {
        console.log("Handling periodic group time check\n, " +
            "current timestamp: ", now,
            "\n pull boundary: ", now + TimeConstants.PULL_BOUNDARY);
        return Promise.all([activateGroups(now),
            sendReminders(now),
            sendNotices(now),
            deactivateGroups(now)])
    }));

function activateGroups(now: number): Promise<void[]> {
    return groupsToActivate(now)
        .then(groups => logGroups("groupsToActivate: ", groups))
        .then((groups: Key<Group>[]) => groups.map((group: Key<Group>) => new GeoFire(database.ref(Paths.GEOMATCHING)
            .child(Paths.GEOMATCHING_GROUPS))
            .set(group.key, [group.val.location.latitude, group.val.location.longitude])
            .then(() => setGroupActivated(group.key))
            .catch(error => {
                console.log(error);
                return
            })))
}

function sendReminders(now: number): Promise<void[]> {
    return groupsToSendMeetingRemindersTo(now)
        .then(groups => logGroups("groupsToSendMeetingRemindersTo: ", groups))
        .then((groups: Key<Group>[]) => Promise.all(groups.map((group: Key<Group>) => getChatInfo(group.key, group.val.groupOwnerId)
            .then(chatInfo => sendPushMessages(chatInfo.activeChatMembersIds,
                pushMessageData(chatInfo, "REMINDER")))
            .then(() => setMeetingReminderSent(group.key))
            .catch(error => {
                console.log(error);
                return
            }))))
}

function sendNotices(now: number): Promise<void[]> {
    return groupsToSendMeetingNoticesTo(now)
        .then(groups => logGroups("groupsToSendMeetingNoticesTo: ", groups))
        .then((groups: Key<Group>[]) => Promise.all(groups.map((group: Key<Group>) => getChatInfo(group.key, group.val.groupOwnerId)
            .then(chatInfo => sendPushMessages(chatInfo.activeChatMembersIds,
                pushMessageData(chatInfo, "NOTICE")))
            .then(() => setMeetingNoticeSent(group.key))
            .catch(error => {
                console.log(error);
                return
            }))))
}

function deactivateGroups(now: number): Promise<void[]> {
    return groupsToDeactivate(now)
        .then(groups => logGroups("groupsToDeactivate: ", groups))
        .then((groups: Key<Group>[]) => groups.map((group: Key<Group>) => new GeoFire(database.ref(Paths.GEOMATCHING)
            .child(Paths.GEOMATCHING_GROUPS))
            .remove(group.key)
            .then(() => setGroupDeactivated(group.key))
            .then(() => Promise.all(groups.map((group: Key<Group>) => getChatInfo(group.key, group.val.groupOwnerId)
                .then(chatInfo => sendPushMessages(chatInfo.activeChatMembersIds,
                    pushMessageData(chatInfo, "DEACTIVATION")))
                .catch(error => {
                    console.log(error);
                    return
                }))))));
}

function groupsToActivate(now: number): Promise<Key<Group>[]> {
    let activateCandidateGroupsMeetingTime = now - TimeConstants.GROUP_ACTIVATION_TIME;
    return groups(activateCandidateGroupsMeetingTime + TimeConstants.PULL_BOUNDARY,
        activateCandidateGroupsMeetingTime)
        .then((groups: Key<Group>[]) => groups.filter((group: Key<Group>) => !group.val.activated))
}

function groupsToSendMeetingRemindersTo(now: number): Promise<Key<Group>[]> {
    let meetingReminderCandidateGroupsMeetingTime = now - TimeConstants.GROUP_MEETING_REMINDER_TIME;
    return groups(meetingReminderCandidateGroupsMeetingTime + TimeConstants.PULL_BOUNDARY,
        meetingReminderCandidateGroupsMeetingTime)
        .then((groups: Key<Group>[]) => groups.filter((group: Key<Group>) => !group.val.meetingReminderSent))
}

function groupsToSendMeetingNoticesTo(now: number): Promise<Key<Group>[]> {
    let meetingNoticeCandidateGroupsMeetingTime = now - TimeConstants.GROUP_MEETING_TIME;
    return groups(meetingNoticeCandidateGroupsMeetingTime + TimeConstants.PULL_BOUNDARY,
        meetingNoticeCandidateGroupsMeetingTime)
        .then((groups: Key<Group>[]) => groups.filter((group: Key<Group>) => !group.val.meetingNoticeSent))
}

function groupsToDeactivate(now: number): Promise<Key<Group>[]> {
    let deactivateCandidateGroupsMeetingTime = now - TimeConstants.GROUP_DEACTIVATION_TIME;
    return groups(deactivateCandidateGroupsMeetingTime + TimeConstants.PULL_BOUNDARY,
        deactivateCandidateGroupsMeetingTime)
        .then((groups: Key<Group>[]) => groups.filter((group: Key<Group>) => !group.val.deactivated))
}

function groups(startAt: number, endAt: number): Promise<Key<Group>[]> {
    return database.ref(Paths.GROUPS)
        .orderByChild("meetingTime")
        .startAt(startAt)
        .endAt(endAt)
        .once("value")
        .then((groupsSnapshot: DataSnapshot) => {
            let groups: Key<Group>[] = [];
            groupsSnapshot.forEach((childSnapshot: DataSnapshot) => {
                groups.push(new Key(childSnapshot.key, childSnapshot.val()));
                return false;
            });
            return groups;
        })
}

function setMeetingReminderSent(groupId: string): Promise<void> {
    return database.ref(Paths.GROUPS)
        .child(groupId)
        .child("meetingReminderSent")
        .set(true)
}

function setMeetingNoticeSent(groupId: string): Promise<void> {
    return database.ref(Paths.GROUPS)
        .child(groupId)
        .child("meetingNoticeSent")
        .set(true)
}

function setGroupActivated(groupId: string): Promise<void> {
    let geolocationStatusIndicators = {
        "discoverable": true,
        "activated": true
    };
    return database.ref(Paths.GROUPS)
        .child(groupId)
        .update(geolocationStatusIndicators)
}

function setGroupDeactivated(groupId: string): Promise<void> {
    let geolocationStatusIndicators = {
        "discoverable": false,
        "deactivated": true
    };
    return database.ref(Paths.GROUPS)
        .child(groupId)
        .update(geolocationStatusIndicators)
}

function pushMessageData(chatInfo: ChatInfo, groupMeetingNotificationType: GroupMeetingNotificationType) {
    return {
        data: {
            group_meeting_notification_data_key: JSON.stringify(new GroupMeetingNotification(chatInfo.chatId,
                chatInfo.chatName,
                chatInfo.photos,
                groupMeetingNotificationType,
                TimeConstants.GROUP_ACTIVATION_TIME,
                TimeConstants.GROUP_MEETING_REMINDER_TIME,
                TimeConstants.GROUP_MEETING_TIME,
                TimeConstants.GROUP_DEACTIVATION_TIME))
        }
    };
}


class GroupMeetingNotification {
    constructor(readonly groupId: string,
                readonly groupName: string,
                readonly photos: PhotoInfo[],
                readonly notificationType: GroupMeetingNotificationType,
                readonly activateTime: number,
                readonly reminderTime: number,
                readonly noticeTime: number,
                readonly deactivateTime: number) {
    }
}

function logGroups(log: string, groups: Key<Group>[]): Key<Group>[] {
    console.log(log, groups);
    return groups;
}







