/**
 * Created by maciek on 05.04.17.
 */
import * as functions from 'firebase-functions';
import * as admin from "firebase-admin";
import {
    ChatType, Message, Paths, ImageContent, Key, ContentType,
    ParticipationPersonAddedContent, ParticipationPersonRemovedContent, GroupTimeSetContent,
    GroupLocationSetContent
} from './model'
import {sendPushMessages} from './pushMessaging'
import {childrenWithKeys} from "./utils";
import {ChatInfo, getChatInfo, getUserName, MessageInfo, PhotoInfo} from "./notificationData";
import {EventContext} from "firebase-functions";
import {Change} from "firebase-functions";

type DataSnapshot = admin.database.DataSnapshot;
const database: admin.database.Database = admin.database();

export let handleNewMessage = functions.database.ref('/messages/{chatId}/{messageId}')
    .onWrite((change, context) => handleMessage(change, context));

function handleMessage(change: Change<DataSnapshot>, context: EventContext) {
    let chatId: string = context.params.chatId;
    let messageId: string = context.params.messageId;
    let message: Message = change.after.val();
    let justCreated = !change.before.exists();
    if (shouldSendNotifications(message, messageId, chatId, justCreated)) {
        return Promise.all([getChatInfo(chatId, message.userId), getLastMessagesInfo(chatId, message)])
            .then(values => {
                let chatInfo: ChatInfo = values[0];
                let lastMessages: MessageInfo[] = values[1];

                let notificationContent = new NewChatMessagePushNotification(chatInfo.chatId, chatInfo.chatName,
                    chatInfo.chatType, chatInfo.photos, lastMessages, messageId, message.content.contentType, message.userId);
                let pushNotification = {
                    data: {
                        new_message_data_key: JSON.stringify(notificationContent)
                    }
                };

                //We should not send notification to sending user
                let recepientsIds = chatInfo.activeChatMembersIds
                    .filter(uId => filterOutRecipients(uId, chatInfo, message));

                let pushNotificationsPromise = sendPushMessages(recepientsIds, pushNotification);
                let updateMembershipsTimestampsPromise = updateMembershipTimestampsIfNeeded(chatId, message, chatInfo.activeChatMembersIds);

                return Promise.all([pushNotificationsPromise, updateMembershipsTimestampsPromise])
            })
    } else {
        return Promise.resolve()
    }
}


function getLastMessagesInfo(chatId: string, newMessage: Message): Promise<MessageInfo[]> {
    return getLastMessages(chatId)
        .then(lastMessages => {
            // TODO Optimize to download each name only once
            let messsagesInfoPromises = lastMessages
                .filter(filterOutMessages)
                .filter(message => message.val.timestamp <= newMessage.timestamp)
                .map(message => getUserName(message.val.userId).then(userName => new MessageInfo(userName, message.key, message.val.timestamp)));
            return Promise.all(messsagesInfoPromises)
        });
}


function getLastMessages(chatId: string): Promise<Key<Message>[]> {
    return database.ref("messages")
        .child(chatId)
        .orderByChild("timestamp")
        //Well, filtering has to be done next in firebase
        .limitToLast(6)
        .once("value")
        .then((snapshot: DataSnapshot) => childrenWithKeys<Message>(snapshot))
}

function updateMembershipTimestampsIfNeeded(chatId: string, message: Message, activeMembersIds): Promise<void[]> {
    switch (message.content.contentType) {
        case "INITIAL_MESSAGE":
        case "PARTICIPATION_PERSON_DELETED_ACCOUNT" :
        case "UNKNOWN":
            return Promise.resolve([]);
        case "GROUP_TIME_SET" :
            if ((message.content as GroupTimeSetContent).initial)
                return Promise.resolve([]);
            else
                return Promise.all([updateUserAggregatedMembershipsTimestamps(chatId, activeMembersIds)
                    , updateChatAggregatedMembershipTimestamp(chatId, message.userId)]);
        case "GROUP_LOCATION_SET" :
            if ((message.content as GroupLocationSetContent).initial)
                return Promise.resolve([]);
            else
                return Promise.all([updateUserAggregatedMembershipsTimestamps(chatId, activeMembersIds)
                    , updateChatAggregatedMembershipTimestamp(chatId, message.userId)]);
        case "TEXT" :
        case "REMOVED" :
        case "PARTICIPATION_PERSON_ADDED" :
        case "PARTICIPATION_PERSON_REMOVED" :
        case "PARTICIPATION_PERSON_LEFT" :
        case "GROUP_NAME_SET" :
        case "GROUP_DESCRIPTION_SET" :
        case "GROUP_IMAGE_SET" :
        case "IMAGE" :
        default :
            return Promise.all([updateUserAggregatedMembershipsTimestamps(chatId, activeMembersIds)
                , updateChatAggregatedMembershipTimestamp(chatId, message.userId)]);
    }
}


function updateUserAggregatedMembershipsTimestamps(chatId: string, activeMembersIds: string[]): Promise<void> {
    let paths: string[] = activeMembersIds.map(uId => `${uId}/${chatId}/lastInteraction`);
    let update = {};
    paths.forEach(path => update[path] = admin.database.ServerValue.TIMESTAMP);
    return database.ref(Paths.MEMBERSHIPS_USER_AGGREGATED)
        .update(update);
}

function updateChatAggregatedMembershipTimestamp(chatId: string, senderId: string): Promise<void> {
    return database.ref(Paths.MEMBERSHIPS_CHAT_AGGREGATED)
        .child(chatId)
        .child(senderId)
        .child("lastInteraction")
        .set(admin.database.ServerValue.TIMESTAMP);
}

function shouldSendNotifications(message: Message, messageId: string, chatId: string, created: boolean): boolean {
    let sendingNotifications: boolean;
    switch (message.content.contentType) {
        case "INITIAL_MESSAGE":
            sendingNotifications = false;
            break;
        case "IMAGE":
            let imageContent: ImageContent = message.content as ImageContent;
            sendingNotifications = imageContent.uploadStatus === "FINISHED";
            break;
        case "GROUP_TIME_SET" :
            sendingNotifications = (message.content as GroupTimeSetContent).initial !== true;
            break;
        case "GROUP_LOCATION_SET" :
            sendingNotifications = (message.content as GroupLocationSetContent).initial !== true;
            break;
        case "PARTICIPATION_PERSON_ADDED":
        case "PARTICIPATION_PERSON_REMOVED":
            sendingNotifications = true;
            break;
        case "PARTICIPATION_PERSON_LEFT":
        case "PARTICIPATION_PERSON_DELETED_ACCOUNT":
            sendingNotifications = false;
            break;
        case "REMOVED":
            sendingNotifications = false;
            break;
        default:
            sendingNotifications = created
    }
    console.log(message.content.contentType + " message with id: " + messageId + " from chat: " + chatId +
        "changed, sending notifications: " + sendingNotifications);
    return sendingNotifications
}

function filterOutMessages(message: Key<Message>): boolean {
    switch (message.val.content.contentType) {
        case "TEXT" :
        case "REMOVED" :
        case "PARTICIPATION_PERSON_ADDED" :
        case "PARTICIPATION_PERSON_REMOVED" :
        case "PARTICIPATION_PERSON_LEFT" :
        case "PARTICIPATION_PERSON_DELETED_ACCOUNT" :
        case "GROUP_TIME_SET" :
        case "GROUP_LOCATION_SET" :
        case "GROUP_NAME_SET" :
        case "GROUP_DESCRIPTION_SET" :
        case "GROUP_IMAGE_SET" :
            return true;
        case "IMAGE" :
            let imageContent: ImageContent = message.val.content as ImageContent;
            return imageContent.uploadStatus === "FINISHED";
        case "INITIAL_MESSAGE":
        case "UNKNOWN":
        default :
            return false
    }
}

function filterOutRecipients(uId: string, chatInfo: ChatInfo, newMessage: Message): boolean {
    switch (newMessage.content.contentType) {
        case "PARTICIPATION_PERSON_ADDED" :
            if (chatInfo.chatType == "DIALOG") {
                return false
            } else {
                let personAddedContent: ParticipationPersonAddedContent = newMessage.content as ParticipationPersonAddedContent;
                //We shouldn't send notification to person that added new user (he knows) and person that
                // was added (he will receive separate notification about it). For all other it should be just
                // another message
                return uId !== personAddedContent.addedPersonId &&
                    uId !== newMessage.userId
            }
        case "PARTICIPATION_PERSON_REMOVED" :
            if (chatInfo.chatType == "DIALOG") {
                return false
            } else {
                let personRemovedContent: ParticipationPersonRemovedContent = newMessage.content as ParticipationPersonRemovedContent;
                //We shouldn't send notification to person that removed the user (he knows) and
                // person that was removed (he will receive separate notification about it). For all other it should be
                // just another message
                return uId !== personRemovedContent.removedPersonId &&
                    uId !== newMessage.userId
            }
        default :
            return uId !== newMessage.userId
    }
}

class NewChatMessagePushNotification {
    constructor(readonly chatId: string,
                readonly chatName: string,
                readonly chatType: ChatType,
                readonly photos: PhotoInfo[],
                readonly messages: MessageInfo[],
                //Deprecated values
                readonly messageId: string,
                readonly contentType: ContentType,
                readonly sendingUserId: string) {
    }
}

