/**
 * Created by maciek on 05.04.17.
 */

import * as functions from 'firebase-functions';
import * as admin from "firebase-admin";

import {ChatVisibleChatMembership, MembershipStatus, Paths, UserVisibleChatMembership} from './model'
import {EventContext} from "firebase-functions";
import {Change} from "firebase-functions";

type DataSnapshot = admin.database.DataSnapshot;
const database: admin.database.Database = admin.database();


export let onChatMembershipCreated = functions.database.ref('/' + Paths.MEMBERSHIPS_CHAT_AGGREGATED + '/{chatId}/{uId}')
    .onCreate((snapshot, context) => aggregateChatMembershipByUser(context.params.chatId, context.params.uId, snapshot.val()));

export let onChatMembershipStatusUpdated = functions.database.ref('/' + Paths.MEMBERSHIPS_CHAT_AGGREGATED + '/{chatId}/{uId}/membershipStatus')
    .onUpdate((change, context) => updateChatMembershipAggregatedByUser(change, context));

export let onChatMembershipDeleted = functions.database.ref('/' + Paths.MEMBERSHIPS_CHAT_AGGREGATED + '/{chatId}/{uId}')
    .onDelete((snapshot, context) => {
        let chatId: string = context.params.chatId;
        let uId: string = context.params.uId;
        return database.ref(Paths.MEMBERSHIPS_USER_AGGREGATED)
            .child(uId)
            .child(chatId)
            .remove()
            .then(() => Promise.resolve('Chat membership for uId ' + uId + ' and chatId ' + chatId + ' removed'));
    });


function updateChatMembershipAggregatedByUser(change: Change<DataSnapshot>, context: EventContext) {
    const chatId: string = context.params.chatId;
    const uId: string = context.params.uId;
    const membership: MembershipStatus = change.after.val();
    console.log('Chat membership status for uId ', uId, ' and chatId ', chatId, ' changed');
    return copyMembershipStatus(chatId, uId, membership)
        .then(() => setLastSeenMessage(chatId, uId, membership))
}

function aggregateChatMembershipByUser(chatId: string, uId: string, chatVisibleMembership: ChatVisibleChatMembership): Promise<void> {
    console.log('aggregateChatMembershipByUser for chatId: ', chatId, " and uId: ", uId, 'with: ', chatVisibleMembership);
    // Timestamp will be undefined if empty. We want it to be null
    let leaveTimestamp: any;
    if (chatVisibleMembership.leaveTimestamp) {
        leaveTimestamp = chatVisibleMembership.leaveTimestamp;
    } else {
        leaveTimestamp = null;
    }

    let userVisibleMembership: UserVisibleChatMembership = new UserVisibleChatMembership(chatVisibleMembership.uId,
        chatVisibleMembership.chatId, chatVisibleMembership.chatType, chatVisibleMembership.membershipStatus,
        leaveTimestamp, admin.database.ServerValue.TIMESTAMP);
    return database.ref(Paths.MEMBERSHIPS_USER_AGGREGATED)
        .child(uId)
        .child(chatId)
        .set(userVisibleMembership);
}

function copyMembershipStatus(chatId: string, uId: string, membershipStatus: MembershipStatus): Promise<void> {
    let updatedValues = {
        [uId + "/" + chatId + "/membershipStatus"]: membershipStatus,
        [uId + "/" + chatId + "/lastInteraction"]: admin.database.ServerValue.TIMESTAMP
    };
    return database.ref(Paths.MEMBERSHIPS_USER_AGGREGATED)
        .update(updatedValues)
}

function setLastSeenMessage(chatId: string, uId: string, membershipStatus: MembershipStatus): Promise<void> {
    if (membershipStatus === "ACTIVE") {
        return database.ref("messages")
            .child(chatId)
            .orderByChild("timestamp")
            .limitToLast(1)
            .once("child_added")
            .then((lastMessageSnapshot: DataSnapshot) => {
                console.log("Last message in chat: ", lastMessageSnapshot.val());
                if (lastMessageSnapshot.exists()) {
                    return database.ref("last_seen_messages").child(chatId).child(uId)
                        .set(lastMessageSnapshot.key);
                } else {
                    //Lexicographical beginning
                    return database.ref("last_seen_messages").child(chatId).child(uId)
                        .set("");
                }
            });
    } else {
        return Promise.resolve();
    }

}