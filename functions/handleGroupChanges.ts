/**
 * Created by maciek on 06.04.17.
 */
import * as functions from 'firebase-functions';
import * as admin from "firebase-admin";
import * as firebase from "firebase";

const GeoFire = require('geofire');

import {
    Group, GroupDescriptionSetContent, GroupImageSetContent, GroupJoinRequest, GroupLocationSetContent,
    GroupNameSetContent, GroupTimeSetContent, InitialMessageContent, Key,
    Message, MessageContent, Paths
} from './model'
import {now} from "./utils";
import {TimeConstants} from "./subscribeToPeriodicGroupTimeCheckTick";
import {getUserName} from "./notificationData";

import {Change, EventContext} from "firebase-functions";

type DataSnapshot = firebase.database.DataSnapshot;

const database: admin.database.Database = admin.database();

export let handleGroupChanges = functions.database.ref('/groups/{groupId}')
    .onWrite((change, context) => handleGroupDataChanged(change, context));


function handleGroupDataChanged(change: Change<DataSnapshot>, context: EventContext): Promise<void> {
    const groupId: string = context.params.groupId;
    let previousDataSnapshot = change.before;
    let currentDataSnapshot = change.after;
    let previousData: Key<Group> = new Key(previousDataSnapshot.key, previousDataSnapshot.val());
    let currentData: Key<Group> = new Key(previousDataSnapshot.key, currentDataSnapshot.val());

    console.log('previousData: ', previousData, ', currentData:', currentData);

    let resultPromise = Promise.resolve();
    if (!previousDataSnapshot.exists()) {
        let newInitialMessagePromise = getUserName(currentData.val.lastEditorId)
            .then(senderName => new Message(currentData.val.lastEditorId, senderName, new InitialMessageContent(),
                currentData.val.groupCreationTimestamp))
            .then(message => sendMessage(groupId, message));
        let newTimeMessagePromise = sendContent(groupId, currentData.val.lastEditorId,
            new GroupTimeSetContent(currentData.val.meetingTime, true));
        let newLocationMessagePromise = sendContent(groupId, currentData.val.lastEditorId,
            new GroupLocationSetContent(currentData.val.location, true));
        let updateGeolocationPromise: Promise<number> = updateGroupGeolocation(currentData);
        resultPromise = Promise.all([newInitialMessagePromise, newTimeMessagePromise, newLocationMessagePromise,
            updateGeolocationPromise])
            .then(() => Promise.resolve());
    } else {
        if (currentData.val.name !== previousData.val.name) {
            let newMessagePromise = sendContent(groupId, currentData.val.lastEditorId, new GroupNameSetContent(currentData.val.name));
            resultPromise = resultPromise.then(() => newMessagePromise);
        }
        if (currentData.val.image !== previousData.val.image) {
            let newMessagePromise = sendContent(groupId, currentData.val.lastEditorId, new GroupImageSetContent(currentData.val.image));
            resultPromise = resultPromise.then(() => newMessagePromise);
        }
        if (currentData.val.description !== previousData.val.description) {
            let newMessagePromise = sendContent(groupId, currentData.val.lastEditorId, new GroupDescriptionSetContent(currentData.val.description));
            resultPromise = resultPromise.then(() => newMessagePromise);
        }
        if (JSON.stringify(currentData.val.location) !== JSON.stringify(previousData.val.location)) {
            let newMessagePromise = sendContent(groupId, currentData.val.lastEditorId, new GroupLocationSetContent(currentData.val.location, null));
            //Geolocation and discoverable status is updated here, don't remove it when creating group
            let updateGeolocationPromise: Promise<number> = updateGroupGeolocation(currentData);
            resultPromise = resultPromise.then(() => Promise.all([updateGeolocationPromise, newMessagePromise])
                .then(() => Promise.resolve()));
        }
        if (currentData.val.meetingTime !== previousData.val.meetingTime) {
            let newMessagePromise = sendContent(groupId, currentData.val.lastEditorId, new GroupTimeSetContent(currentData.val.meetingTime, null));
            //Geolocation and discoverable status is updated here, don't remove it when creating group
            let updateGeolocationPromise: Promise<number> = updateGroupGeolocation(currentData);
            resultPromise = resultPromise.then(() =>
                Promise.all([updateGeolocationPromise, newMessagePromise])
                    .then(() => Promise.resolve()));
        }
        if (currentData.val.discoverable !== previousData.val.discoverable) {
            resultPromise = resultPromise.then(() => redeemJoinRequests(currentData.key).then(() => Promise.resolve()));
        }
    }
    return resultPromise;
}

function sendContent(chatId: string, senderId: string, content: MessageContent): Promise<void> {
    return getUserName(senderId)
        .then(senderName => new Message(senderId, senderName, content, admin.database.ServerValue.TIMESTAMP))
        .then(message => sendMessage(chatId, message))
}

function sendMessage(chatId: string, message: Message): Promise<void> {
    return database.ref("messages")
        .child(chatId)
        .push()
        .set(message);
}

function updateGroupGeolocation(group: Key<Group>): Promise<number> {
    return now()
        .then((now: number) => {
            let groupActivated = now > group.val.meetingTime + TimeConstants.GROUP_ACTIVATION_TIME;
            let reminderSent = now > group.val.meetingTime + TimeConstants.GROUP_MEETING_REMINDER_TIME;
            let noticeSent = now > group.val.meetingTime + TimeConstants.GROUP_MEETING_TIME;
            let groupDeactivated = now > group.val.meetingTime + TimeConstants.GROUP_DEACTIVATION_TIME;
            let discoverable = groupActivated && !groupDeactivated;
            console.log(`now: ${now}, group.val.meetingTime: ${group.val.meetingTime}, groupActivated: ${groupActivated}, reminderSent: ${reminderSent}, noticeSent: ${noticeSent}, groupDeactivated: ${groupDeactivated}, discoverable: ${discoverable}`);
            return updateGroupGeolocationStatusIndicators(group.key,
                discoverable, groupActivated, reminderSent, noticeSent, groupDeactivated)
                .then(() => {
                    if (discoverable) {
                        return new GeoFire(database.ref(Paths.GEOMATCHING)
                            .child(Paths.GEOMATCHING_GROUPS))
                            .set(group.key, [group.val.location.latitude, group.val.location.longitude]);
                    } else {
                        return new GeoFire(database.ref(Paths.GEOMATCHING)
                            .child(Paths.GEOMATCHING_GROUPS))
                            .remove(group.key);
                    }

                });
        })
}

function updateGroupGeolocationStatusIndicators(groupId: string,
                                                discoverable: boolean,
                                                activated: boolean,
                                                meetingReminderSent: boolean,
                                                meetingNoticeSent: boolean,
                                                deactivated: boolean): Promise<void> {
    let geolocationStatusIndicators = {
        "discoverable": discoverable,
        "activated": activated,
        "meetingReminderSent": meetingReminderSent,
        "meetingNoticeSent": meetingNoticeSent,
        "deactivated": deactivated
    };

    return database.ref(Paths.GROUPS)
        .child(groupId)
        .update(geolocationStatusIndicators)
}

function redeemJoinRequests(groupId: string): Promise<void[]> {
    return getGroupJoinRequests(groupId)
        .then(joinRequests => joinRequests.filter(joinRequest =>
            joinRequest.val.status === "NOT_INTERESTED" ||
            joinRequest.val.status === "REMOVED" ||
            joinRequest.val.status === "LEFT"))
        .then(joinRequests => {
            return joinRequests.map(joinRequest => database.ref(Paths.GROUP_JOIN_REQUESTS)
                .child(groupId)
                .child(joinRequest.key)
                .child("status")
                .set("NONE"))
        })
        .then(promises => Promise.all(promises))
}


function getGroupJoinRequests(groupId: string): Promise<Key<GroupJoinRequest>[]> {
    return database.ref(Paths.GROUP_JOIN_REQUESTS).child(groupId)
        .once("value")
        .then((snapshot: DataSnapshot) => {
            let joinRequests: Key<GroupJoinRequest>[] = [];
            snapshot.forEach((childSnapshot) => {
                joinRequests.push(new Key(childSnapshot.key, childSnapshot.val()));
                return false;
            });
            return joinRequests;
        })
}