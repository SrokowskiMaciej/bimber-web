#!/usr/bin/env bash
if [ ! -f $PROJECT_DIR/functions/credentials/$GCLOUD_PROJECT.json ]; then
    echo "Credentials file not existing! Put correct credentials file under functions/credentials/$GCLOUD_PROJECT.json "
    exit 1
fi
if [ ! -f $PROJECT_DIR/functions/credentials/$GCLOUD_PROJECT-migration-auth-token.txt ]; then
    echo "Migration password does not exists! Put correct password file under functions/credentials/$GCLOUD_PROJECT-migration-auth-token.txt"
    exit 1
fi
exit 0
