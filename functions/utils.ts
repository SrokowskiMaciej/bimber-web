/**
 * Created by maciek on 24.07.17.
 */

import * as admin from "firebase-admin";
import {UserRecord} from "firebase-functions/lib/providers/auth";
import {Key} from "./model";
type DataSnapshot = admin.database.DataSnapshot;
const database: admin.database.Database = admin.database();

export function now(): Promise<number> {
    return database.ref("/.info/serverTimeOffset")
        .once("value")
        .then((snapshot: DataSnapshot) => Date.now() + snapshot.val())
}

export function getUsersFullName(userRecord: UserRecord): string {

    if (userRecord.displayName) {
        return userRecord.displayName;
    } else {
        return "Unknown user";
    }
}

export function getUsersDisplayName(userRecord: UserRecord): string {
    if (userRecord.displayName) {
        let nonUppercasedName = userRecord.displayName.split(" ")[0];
        return nonUppercasedName.charAt(0).toUpperCase() + nonUppercasedName.slice(1);
    } else {
        return "Unknown user";
    }
}

export function children<T>(snapshot: DataSnapshot): Promise<T[]> {
    let children: T[] = [];
    snapshot.forEach(childSnapshot => {
        children.push(childSnapshot.val());
        return false
    });
    return Promise.resolve(children)
}

export function childrenWithKeys<T>(snapshot: DataSnapshot): Promise<Key<T>[]> {
    let children: Key<T>[] = [];
    snapshot.forEach(childSnapshot => {
        children.push(new Key(childSnapshot.key, childSnapshot.val()));
        return false
    });
    return Promise.resolve(children)
}