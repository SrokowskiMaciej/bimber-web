/**
 * Created by maciek on 31.10.17.
 */
let fs = require('fs-extra');

class Credentials {
    public static readonly STAGING_PROJECT_ID: string = "bimber-staging"
    public static readonly STAGING_PROJECT_CREDENTIALS_PATH: string = "credentials/bimber-staging.json"
    public static readonly STAGING_PROJECT_MIGRATION_PASSWORD_PATH: string = "credentials/bimber-staging-migration-auth-token.txt"
    public static readonly PRODUCTION_PROJECT_ID: string = "bimber-a8ebe"
    public static readonly PRODUCTION_PROJECT_CREDENTIALS_PATH: string = "credentials/bimber-a8ebe.json"
    public static readonly PRODUCTION_PROJECT_MIGRATION_PASSWORD_PATH: string = "credentials/bimber-a8ebe-migration-auth-token.txt"

}

export function getProjectId() {
    return process.env.GCLOUD_PROJECT
}

export function getProjectCredentialsFilePath() {
    let projectId = getProjectId();
    switch (projectId) {
        case Credentials.PRODUCTION_PROJECT_ID:
            return Credentials.PRODUCTION_PROJECT_CREDENTIALS_PATH;
        case Credentials.STAGING_PROJECT_ID:
            return Credentials.STAGING_PROJECT_CREDENTIALS_PATH;
        default:
            throw new Error("Unknown project id: " + projectId);
    }
}

export function getProjectMigrationAuthToken() {
    let projectId = getProjectId();
    switch (projectId) {
        case Credentials.PRODUCTION_PROJECT_ID:
            return fs.readFileSync(Credentials.PRODUCTION_PROJECT_MIGRATION_PASSWORD_PATH, "utf8");
        case Credentials.STAGING_PROJECT_ID:
            return fs.readFileSync(Credentials.STAGING_PROJECT_MIGRATION_PASSWORD_PATH, "utf8");
        default:
            throw new Error("Unknown project id: " + projectId);
    }
}