/**
 * Created by maciek on 05.04.17.
 */

import * as functions from 'firebase-functions';
import * as admin from "firebase-admin";

import {InitialUserCredentials, Paths, Photo} from './model'
import {getProjectCredentialsFilePath, getProjectId} from "./credentialsProvider";

let request = require('request-promise-native');
let gm = require('gm').subClass({imageMagick: true});
let path = require('path');
let os = require('os');
let fs = require('fs-extra');
let download = require('image-downloader')

const database: admin.database.Database = admin.database();
const Storage = require('@google-cloud/storage');
const appendQuery = require('append-query');

const storage = new Storage({
    projectId: getProjectId(),
    keyFilename: getProjectCredentialsFilePath(),
});

export let onInitialUserCredentials = functions.database.ref('/' + Paths.INITIAL_USER_CREDENTALS + '/{uId}')
    .onCreate((snap, context) => {
        let uId: string = context.params.uId;
        let credentials: InitialUserCredentials = snap.val();
        let newPhotoDatabaseReference = database.ref("photos").child(uId).push();
        let newPhotoId = newPhotoDatabaseReference.key;

        let newPhotoFileName = newPhotoDatabaseReference.toString().substring(newPhotoDatabaseReference.root.toString().length);
        let tempLocalFile = path.normalize(path.join(os.tmpdir(), newPhotoFileName)) + ".jpg";

        switch (credentials.providerType) {
            case "facebook.com":
                return fs.ensureDir(path.dirname(tempLocalFile))
                    .then(() => downloadFacebookPicture(credentials, tempLocalFile))
                    .then(() => uploadProfilePicture(tempLocalFile, uId, newPhotoId))
                    .catch(error => {
                        console.error(`Failed to obtain facebook picture for user: ${uId}, resuming with default`, error);
                        return getFallbackPicture(uId);
                    })
                    .then(uploadedPictureUrl => newPhotoDatabaseReference.set(new Photo(uploadedPictureUrl
                        , "FINISHED", 0)))
                    .then(() => fs.unlinkSync(tempLocalFile));
            case "google.com":
                return fs.ensureDir(path.dirname(tempLocalFile))
                    .then(() => downloadGooglePicture(uId, tempLocalFile))
                    .then(() => uploadProfilePicture(tempLocalFile, uId, newPhotoId))
                    .catch(error => {
                        console.error(`Failed to obtain google picture for user: ${uId}, resuming with default`, error);
                        return getFallbackPicture(uId);
                    })
                    .then(uploadedPictureUrl => newPhotoDatabaseReference.set(new Photo(uploadedPictureUrl
                        , "FINISHED", 0)))
                    .then(() => fs.unlinkSync(tempLocalFile));
            default:
                return Promise.reject(`Unknown provider type: ${credentials.providerType}`)
        }
    });

function downloadFacebookPicture(credentials: InitialUserCredentials, file: string): Promise<void> {
    let requestOptions = {
        uri: "https://graph.facebook.com/me",
        qs: {
            fields: "picture.width(500).height(500),age_range"
        },
        headers: {
            Authorization: `OAuth ${credentials.token}`
        },
        json: true
    };
    return request(requestOptions)
        .then(response => {
            console.log(`Response: ${JSON.stringify(response)}`);
            return response;
        })
        .then(response => {
            let shorterDimension = Math.min(response.picture.data.width, response.picture.data.width);
            const imageOptions = {
                url: response.picture.data.url,
                dest: os.tmpdir()                  // Save to /path/to/dest/image.jpg
            };
            return download.image(imageOptions)
                .then(({filename, image}) => {
                    return new Promise(((resolve, reject) => {
                        gm(filename)
                            .thumb(shorterDimension, shorterDimension, file, 100, function (err) {
                                if (!err) {
                                    console.log("Resized image");
                                    resolve("Success")
                                } else {
                                    console.log("Not resized image: " + err);
                                    reject(err)
                                }
                            })
                    }))
                        .then(() => fs.unlinkSync(filename));
                })
        })
}

function downloadGooglePicture(uId: string, file: string): Promise<void> {
    return admin.auth().getUser(uId)
    //In case of google there is no need to make another call to their api, provided url is good just add size query param to it
        .then(userRecord => appendQuery(userRecord.photoURL, {sz: 500}))
        .then(pictureUrl => {
            const imageOptions = {
                url: pictureUrl,
                dest: os.tmpdir()                  // Save to /path/to/dest/image.jpg
            };
            return download.image(imageOptions)
                .then(({filename, image}) => {
                    return new Promise(((resolve, reject) => {
                        gm(filename)
                            .thumb(500, 500, file, 100, function (err) {
                                if (!err) {
                                    console.log("Resized image");
                                    resolve("Success")
                                } else {
                                    console.log("Not resized image: " + err);
                                    reject(err)
                                }
                            })
                    }))
                        .then(() => fs.unlinkSync(filename));
                })
        })
}

function getFallbackPicture(uId: string): Promise<string> {
    return admin.auth().getUser(uId)
        .then(userRecord => userRecord.photoURL)
}

function uploadProfilePicture(file: string, uId: string, pictureId: string): Promise<string> {
    let bucket = storage.bucket(JSON.parse(process.env.FIREBASE_CONFIG).storageBucket);
    let storageFilePath = `photos/profiles/${uId}/${pictureId}`;
    let storageFile = bucket.file(storageFilePath);
    return bucket.upload(file, {destination: storageFilePath})
        .then(() => {
            const config = {
                action: 'read',
                expires: '03-01-2500'
            };
            return storageFile.getSignedUrl(config)
        })
        .then(results => results[0])
}