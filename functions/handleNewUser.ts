/**
 * Created by maciek on 29.05.17.
 */

import * as functions from 'firebase-functions';
import * as admin from "firebase-admin";
import {User} from "./model";
import {getUsersDisplayName, getUsersFullName} from "./utils";
import UserRecord = admin.auth.UserRecord;

const database: admin.database.Database = admin.database();

export let handleNewUserCreation = functions.auth.user().onCreate(event => {
    return handleNewUserCreated(event);
});

function handleNewUserCreated(userRecord: UserRecord) {
    let displayName: string = getUsersDisplayName(userRecord);
    let fullName: string = getUsersFullName(userRecord);
    let newUser: User = new User(displayName, fullName, "", "MALE", 25);
    let newUserPromise: Promise<void> = database.ref("users").child(userRecord.uid).set(newUser);
    let lastSeenPromise: Promise<void> = database.ref("last_user_activity").child(userRecord.uid).set(admin.database.ServerValue.TIMESTAMP);
    if (userRecord.email) {
        return Promise.all([newUserPromise, lastSeenPromise, database.ref("emails").child(userRecord.uid).set(userRecord.email)]);
    } else {
        return Promise.all([newUserPromise, lastSeenPromise])
    }
}