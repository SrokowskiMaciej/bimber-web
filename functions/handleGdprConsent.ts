/**
 * Created by maciek on 06.04.17.
 */
import * as functions from 'firebase-functions';
import * as admin from "firebase-admin";
import * as firebase from "firebase";

const GeoFire = require('geofire');

import {
    GdprConsent, Paths
} from './model'

import {Change, EventContext} from "firebase-functions";

type DataSnapshot = firebase.database.DataSnapshot;

export let handleGdprConsent = functions.database.ref('/' + Paths.GDPR_CONSENTS + '/{uId}')
    .onWrite((change, context) => handleGdprConsentChanged(change, context));


function handleGdprConsentChanged(change: Change<DataSnapshot>, context: EventContext): Promise<void> {
    const uId: string = context.params.uId;
    let currentDataSnapshot = change.after;
    let currentData: GdprConsent = currentDataSnapshot.val();

    switch (currentData) {
        case "REFUSED":
            return admin.auth().deleteUser(uId);
        default:
            return Promise.resolve()
    }
}