/**
 * Created by maciek on 29.05.17.
 */

import * as functions from 'firebase-functions';
import * as admin from "firebase-admin";
import {ChatVisibleChatMembership, Message, ParticipationPersonDeletedAccountContent, Paths} from "./model";
import {getUsersDisplayName} from "./utils";
import {getProjectMigrationAuthToken} from "./credentialsProvider";
import {getExternalEvaluationUri} from "./uriProvider";


const request = require('request-promise-native')

const database: admin.database.Database = admin.database();

type DataSnapshot = admin.database.DataSnapshot;
export let handleUserDeletion = functions.auth.user().onDelete((userRecord, context) => {
    return handleUserDeleted(userRecord, context);
});

function handleUserDeleted(userRecord: functions.auth.UserRecord, context: functions.EventContext) {
    let uid = userRecord.uid;
    let displayName: string = getUsersDisplayName(userRecord);
    return Promise.all([markUserAsDeleted(uid), deleteMessagingTokens(uid), deleteFavouritePlaces(uid),
        deleteAlcohols(uid), deleteLocations(uid), deletePhotos(uid), leaveChats(uid, displayName),
        deletePersonEvaluations(uid), deleteGroupJoinRequests(uid), removeExternalUserEvaluations(uid)])
        .then(() => deleteUser(uid));
}

function markUserAsDeleted(uId: string) {
    return database.ref(Paths.DELETED_USERS).child(uId).set(true);
}

function deleteMessagingTokens(uId: string) {
    return database.ref("messaging_tokens").child(uId).remove();
}

function deleteAlcohols(uId: string) {
    return database.ref("alcohols").child(uId).remove();
}

function deleteFavouritePlaces(uId: string) {
    return database.ref("favourite_places").child(uId).remove();
}

function deleteUser(uId: string) {
    return database.ref("users").child(uId).remove();
}

function deletePersonEvaluations(uId: string) {
    return database.ref("evaluated_persons").child(uId).remove();
}

function deletePhotos(uId: string) {
    return database.ref("photos").child(uId).remove();
}

function deleteLocations(uId: string): Promise<void> {
    return database.ref("user_locations")
        .child(uId)
        .remove();
}

function leaveChats(uId: string, displayName: string): Promise<void[]> {
    return database.ref(Paths.MEMBERSHIPS_USER_AGGREGATED)
        .child(uId)
        .orderByChild("membershipStatus")
        .equalTo("ACTIVE")
        .once("value")
        .then((dataSnapshot: DataSnapshot) => {
            let leftChatsPromises: Promise<void> [] = [];
            dataSnapshot.forEach((childSnapshot: DataSnapshot) => {
                let chatMembership: ChatVisibleChatMembership = childSnapshot.val();
                let deleteChatMembershipPromise: Promise<void> = database.ref(Paths.MEMBERSHIPS_CHAT_AGGREGATED)
                    .child(chatMembership.chatId)
                    .child(uId)
                    .remove();
                let sendParticipantDeletedAccountMessagePromise: Promise<void> =
                    sendParticipantDeletedAccountMessage(chatMembership.chatId, uId, displayName);
                let pair: Promise<void> = Promise.all([deleteChatMembershipPromise, sendParticipantDeletedAccountMessagePromise])
                    .then(value => Promise.resolve());
                leftChatsPromises.push(pair);
                return false;
            });
            return Promise.all(leftChatsPromises);
        });
}

function deleteGroupJoinRequests(uId: string): Promise<void[]> {
    return database.ref(Paths.GROUP_JOIN_REQUESTS_USER_AGGREGATED)
        .child(uId)
        .once("value")
        .then((dataSnapshot: DataSnapshot) => {
            let deleteGroupJoinRequestPromises: Promise<void> [] = [];
            dataSnapshot.forEach((childSnapshot: DataSnapshot) => {
                let deleteChatMembershipPromise: Promise<void> = database.ref(Paths.GROUP_JOIN_REQUESTS)
                    .child(childSnapshot.key)
                    .child(uId)
                    .remove();
                deleteGroupJoinRequestPromises.push(deleteChatMembershipPromise);
                return false;
            });
            return Promise.all(deleteGroupJoinRequestPromises);
        });
}


function removeExternalUserEvaluations(evaluatingUId: string): Promise<any> {
    let requestOptions = {
        method: 'DELETE',
        uri: getExternalEvaluationUri() + evaluatingUId,
        headers: {
            Authorization: getProjectMigrationAuthToken()
        },
        json: true
    };
    return request(requestOptions)
        .catch(error => {
            console.log("Failed to send evaluation to external service: " + error.toString())
            return
        })
}

function sendParticipantDeletedAccountMessage(chatId: string, participantId: string, displayName: string): Promise<void> {
    let message = new Message(participantId, displayName, new ParticipationPersonDeletedAccountContent(displayName, participantId),
        admin.database.ServerValue.TIMESTAMP);
    return database.ref("messages").child(chatId).push().set(message);
}