/**
 * Created by maciek on 06.04.17.
 */
import * as functions from 'firebase-functions';
import * as admin from "firebase-admin";

import {
    ChatVisibleChatMembership, GroupJoinRequest, Paths, GroupParticipationChange, Message,
    ParticipationPersonAddedContent, ParticipationPersonLeftContent, ParticipationPersonRemovedContent,
    GroupJoinRequestStatus
} from './model'
import {sendPushMessages} from "./pushMessaging";
import {getChatInfo, getUserName, getUserPhoto, PhotoInfo} from "./notificationData";
import {Change, EventContext} from "firebase-functions";

type DataSnapshot = admin.database.DataSnapshot;
const database: admin.database.Database = admin.database();

export let handleGroupParticipantChange = functions.database.ref('/' + Paths.GROUP_JOIN_REQUESTS + '/{groupId}/{participantId}')
    .onWrite((change, context) => handleGroupParticipantChanged(change, context));


function handleGroupParticipantChanged(change: Change<DataSnapshot>, context: EventContext) {
    const groupId: string = context.params.groupId;
    const participantId: string = context.params.participantId;

    let previousDataSnapshot = change.before;
    let currentDataSnapshot = change.after;
    let previousData: GroupJoinRequest = previousDataSnapshot.val();
    let currentData: GroupJoinRequest = currentDataSnapshot.val();

    console.log('previousData ', previousData, ', currentData', currentData);
    if (!currentDataSnapshot.exists()) {
        console.log('User ', participantId, ' deleted join request to group ', groupId);
        return aggregareGroupJoinRequestByUser(groupId, participantId, null)
            .then(() => {
                if (previousDataSnapshot.val().status === "ACCEPTED" || currentData.status === "INITIAL") {
                    return updateGroupMembersCounter(false, groupId);
                } else {
                    return Promise.resolve();
                }
            });
        //This is the same as accepted but doesn't send the message
    } else if (currentData.status === "INITIAL") {
        console.log('Participant ', participantId, ' added to group ', groupId);
        //For all purposes besides sending join message this should behave as ACCEPTED
        let modifiedData = new GroupJoinRequest("ACCEPTED", currentData.lastEditorId);
        return setActiveChatMembership(groupId, participantId)
            .then(() => sendMembershipChangedNotification(groupId, participantId, modifiedData))
            .then(() => aggregareGroupJoinRequestByUser(groupId, participantId, modifiedData))
            .then(() => updateUserPartiesCounter(true, participantId))
            .then(() => updateGroupMembersCounter(true, groupId))
    } else if (currentData.status === "ACCEPTED") {
        console.log('Participant ', participantId, ' added to group ', groupId);
        return setActiveChatMembership(groupId, participantId)
            .then(() => sendParticipantAddedMessage(groupId, participantId, currentData))
            .then(() => sendMembershipChangedNotification(groupId, participantId, currentData))
            .then(() => aggregareGroupJoinRequestByUser(groupId, participantId, currentData))
            .then(() => updateUserPartiesCounter(true, participantId))
            .then(() => updateGroupMembersCounter(true, groupId))
    } else if (currentData.status === "REMOVED") {
        console.log('Participant ', participantId, ' removed from group ', groupId);
        return sendParticipantRemovedMessage(groupId, participantId, currentData)
            .then(() => setExpiredChatMembership(groupId, participantId))
            .then(() => sendMembershipChangedNotification(groupId, participantId, currentData))
            .then(() => aggregareGroupJoinRequestByUser(groupId, participantId, currentData))
            .then(() => updateUserPartiesCounter(false, participantId))
            .then(() => updateGroupMembersCounter(false, groupId))
    } else if (currentData.status === "LEFT") {
        console.log('Participant ', participantId, ' left the group ', groupId);
        return sendParticipantLeftMessage(groupId, participantId, currentData)
            .then(() => setExpiredChatMembership(groupId, participantId))
            .then(() => aggregareGroupJoinRequestByUser(groupId, participantId, currentData))
            .then(() => updateUserPartiesCounter(false, participantId))
            .then(() => updateGroupMembersCounter(false, groupId))
    } else if (currentData.status === "MEMBERSHIP_REQUESTED") {
        console.log('User ', participantId, ' wants to join group ', groupId);
        return sendMembershipRequestedNotifications(groupId, participantId, currentData)
            .then(() => aggregareGroupJoinRequestByUser(groupId, participantId, currentData))
    } else if (currentData.status === "NONE") {
        if (previousDataSnapshot.val().status === "ACCEPTED") {
            return updateGroupMembersCounter(false, groupId);
        } else {
            return Promise.resolve();
        }
    } else if (currentData.status === "NOT_INTERESTED") {
        return Promise.resolve("User not interested");
    } else {
        return Promise.reject("Not expected change");
    }
}

function setActiveChatMembership(groupId: string, participantId: string): Promise<void> {
    let membership = new ChatVisibleChatMembership(participantId, groupId, "GROUP", "ACTIVE", null, null);
    return database.ref(Paths.MEMBERSHIPS_CHAT_AGGREGATED).child(groupId).child(participantId).set(membership);
}

function setExpiredChatMembership(groupId: string, participantId: string): Promise<void> {
    let membership = new ChatVisibleChatMembership(participantId, groupId, "GROUP", "EXPIRED", admin.database.ServerValue.TIMESTAMP, null);
    return database.ref(Paths.MEMBERSHIPS_CHAT_AGGREGATED).child(groupId).child(participantId).set(membership);
}


function sendParticipantAddedMessage(groupId: string, participantId: string, currentData: GroupJoinRequest): Promise<void> {
    return Promise.all([getUserName(currentData.lastEditorId), getUserName(participantId)])
        .then(values => new Message(currentData.lastEditorId, values[0],
            new ParticipationPersonAddedContent(values[1], participantId),
            admin.database.ServerValue.TIMESTAMP))
        .then((message: Message) => database.ref("messages").child(groupId).push(message))
}

function sendParticipantLeftMessage(groupId: string, participantId: string, currentData: GroupJoinRequest): Promise<void> {
    return getUserName(participantId)
        .then((username: string) => new Message(currentData.lastEditorId, username,
            new ParticipationPersonLeftContent(username, participantId),
            admin.database.ServerValue.TIMESTAMP))
        .then((message: Message) => database.ref("messages").child(groupId).push(message))
}

function sendParticipantRemovedMessage(groupId: string, participantId: string, currentData: GroupJoinRequest): Promise<void> {
    return Promise.all([getUserName(currentData.lastEditorId), getUserName(participantId)])
        .then(values => new Message(currentData.lastEditorId, values[0],
            new ParticipationPersonRemovedContent(values[1], participantId),
            admin.database.ServerValue.TIMESTAMP))
        .then((message: Message) => database.ref("messages").child(groupId).push(message))
}


function sendMembershipRequestedNotifications(groupId: string, candidateId: string, currentData: GroupJoinRequest): Promise<void[]> {
    return Promise.all([getChatInfo(groupId, currentData.lastEditorId), getUserName(candidateId), getUserPhoto(candidateId)])
        .then(values => {
            let chatInfo = values[0]
            let candidateName = values[1]
            let candidatePhoto = values[2]
            let pushMessage = new GroupParticipationPushNotification(groupId, chatInfo.chatName, candidateId, candidateName,
                candidatePhoto, candidateId, candidateName, candidatePhoto, currentData.status, chatInfo.photos, currentData);

            let recipientsIds = chatInfo.activeChatMembersIds.filter(uId => uId !== candidateId)
            return sendNotification(recipientsIds, pushMessage)
        })
}

function sendMembershipChangedNotification(groupId: string, candidateId: string, currentData: GroupJoinRequest): Promise<void[]> {
    return Promise.all([getChatInfo(groupId, currentData.lastEditorId),
        getUserName(candidateId),
        getUserPhoto(candidateId),
        getUserName(currentData.lastEditorId),
        getUserPhoto(currentData.lastEditorId)])
        .then(values => {
            let chatInfo = values[0]
            let candidateName = values[1]
            let candidatePhoto = values[2]
            let actingUserName = values[3]
            let actingUserPhoto = values[4]
            let pushMessage = new GroupParticipationPushNotification(groupId, chatInfo.chatName, candidateId, candidateName,
                candidatePhoto, currentData.lastEditorId, actingUserName, actingUserPhoto, currentData.status,
                chatInfo.photos, currentData);
            if (chatInfo.ownerId === candidateId) {
                return Promise.resolve([])
            } else {
                return sendNotification([candidateId], pushMessage)
            }
        })
}

function sendNotification(receiversIds: string[], pushMessage: GroupParticipationPushNotification): Promise<void[]> {
    var message = {
        data: {
            group_participation_data_key: JSON.stringify(pushMessage)
        }
    };
    return sendPushMessages(receiversIds, message);
}

function aggregareGroupJoinRequestByUser(groupId: string, participantId: string, currentData: GroupJoinRequest): Promise<void> {
    return database.ref(Paths.GROUP_JOIN_REQUESTS_USER_AGGREGATED)
        .child(participantId)
        .child(groupId)
        .set(currentData)
}

function updateUserPartiesCounter(increment: boolean, userId): Promise<any> {
    return database.ref("users").child(userId).child("parties").transaction((currentCount: number) => {
        if (increment) {
            return (currentCount || 0) + 1
        } else {
            return (currentCount || 0) - 1
        }
    })
}

function updateGroupMembersCounter(increment: boolean, groupId): Promise<any> {
    return database.ref(Paths.GROUPS).child(groupId).child("membersCount").transaction((currentCount: number) => {
        if (increment) {
            return (currentCount || 0) + 1
        } else {
            return (currentCount || 0) - 1
        }
    })
}

class GroupParticipationPushNotification {
    constructor(readonly groupId: string,
                readonly groupName: string,
                readonly participantId: string,
                readonly participantName: string,
                readonly participantPhoto: string,
                readonly actingUserId: string,
                readonly actingUserName: string,
                readonly actingUserPhoto: string,
                readonly status: GroupJoinRequestStatus,
                readonly photos: PhotoInfo[],
                //Deprecated values
                readonly groupJoinRequest: GroupJoinRequest) {
    }
}