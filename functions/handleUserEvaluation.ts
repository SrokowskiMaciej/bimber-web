'use strict';

import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
import {sendPushMessages} from "./pushMessaging";

const request = require('request-promise-native');

import {
    ChatVisibleChatMembership, InitialMessageContent, Message, ParticipationPersonAddedContent,
    ParticipationPersonLeftContent, Paths,
    PersonEvaluation, PersonEvaluationStatus
} from "./model";
import {getChatInfo, getUserName, getUserPhoto, PhotoInfo} from "./notificationData";
import {getExternalEvaluationUri, getExternalLocationUri} from "./uriProvider";
import {getProjectMigrationAuthToken} from "./credentialsProvider";
import {EventContext} from "firebase-functions";
import {Change} from "firebase-functions";

type DataSnapshot = admin.database.DataSnapshot;

const database: admin.database.Database = admin.database();


export let handleUserEvaluation = functions.database.ref('/evaluated_persons/{evaluatingUserId}/{evaluatedUserId}')
    .onWrite((change, context) => handleEvaluation(change, context));


function handleEvaluation(change: Change<DataSnapshot>, context: EventContext) {
    const evaluatingUserId: string = context.params.evaluatingUserId;
    const evaluatedUserId: string = context.params.evaluatedUserId;

    let previousDataSnapshot: DataSnapshot = change.before;
    let previousPersonEvaluation: PersonEvaluation = previousDataSnapshot.val();

    let currentDataSnapshot: DataSnapshot = change.after;
    let personEvaluation: PersonEvaluation = currentDataSnapshot.val();

    console.log('User evaluated. Evaluating user: ', evaluatingUserId, ", Evaluated user: ", evaluatedUserId);

    if (!currentDataSnapshot.exists()) {
        if (previousDataSnapshot.exists() &&
            previousPersonEvaluation.status === "MATCHED") {
            return updateFriendsCounter(false, evaluatedUserId);
        } else {
            return Promise.resolve('Deleted evaluation');
        }
    } else if (previousDataSnapshot.exists() &&
        previousPersonEvaluation.status === "MATCHED" &&
        personEvaluation.status === "DISLIKED") {
        let handleUnmatchPromise = handleUnmatch(evaluatingUserId, evaluatedUserId, personEvaluation.dialogId);
        let externalUserEvaluationDislikePromise = externalUserEvaluationDislike(evaluatingUserId, evaluatedUserId);
        return Promise.all([externalUserEvaluationDislikePromise, handleUnmatchPromise]);
    } else if (previousDataSnapshot.exists() &&
        previousPersonEvaluation.dialogId &&
        personEvaluation.status === "LIKED") {
        let handleRelikePromise = handleRelike(evaluatingUserId, evaluatedUserId, previousPersonEvaluation, personEvaluation);
        let externalUserEvaluationLikePromise = externalUserEvaluationLike(evaluatingUserId, evaluatedUserId);
        return Promise.all([handleRelikePromise, externalUserEvaluationLikePromise])
    } else if (personEvaluation.status === "LIKED") {
        let handleLikePromise = handleLike(evaluatingUserId, evaluatedUserId);
        let externalUserEvaluationLikePromise = externalUserEvaluationLike(evaluatingUserId, evaluatedUserId);
        return Promise.all([handleLikePromise, externalUserEvaluationLikePromise])
    } else if (personEvaluation.status === "MATCHED" && personEvaluation.dialogId) {
        let handleMatchPromise = handleMatch(personEvaluation, evaluatingUserId, evaluatedUserId);
        let externalUserEvaluationLikePromise = externalUserEvaluationLike(evaluatingUserId, evaluatedUserId);
        return Promise.all([handleMatchPromise, externalUserEvaluationLikePromise])
    } else {
        let handleDislikePromise = Promise.resolve('Not matching users');
        let externalUserEvaluationDislikePromise = externalUserEvaluationDislike(evaluatingUserId, evaluatedUserId);
        return Promise.all([handleDislikePromise, externalUserEvaluationDislikePromise])
    }
}

function handleUnmatch(evaluatingUserId: string, evaluatedUserId: string, dialogId: string): Promise<any> {
    return setLeftChatMember(dialogId, evaluatingUserId)
        .then(() => updateFriendsCounter(false, evaluatingUserId))
        .then(() => sendPersonLeftMessage(dialogId, evaluatingUserId))
        .then(() => setDislikedPersonEvaluations(evaluatingUserId, evaluatedUserId))
}


function handleRelike(evaluatingUserId: string, evaluatedUserId: string, previousPersonEvaluation: PersonEvaluation, personEvaluation: PersonEvaluation) {
    return checkForMatch(evaluatingUserId, evaluatedUserId)
        .then((matched: boolean) => {
            if (matched) {
                return setMatchPersonEvaluations(previousPersonEvaluation.dialogId, evaluatingUserId, evaluatedUserId)
                    .then(() => sendPersonRejoinedMessage(personEvaluation.dialogId, evaluatingUserId));
            } else {
                return
            }

        })
}

function handleLike(evaluatingUserId: string, evaluatedUserId: string) {
    return checkForMatch(evaluatingUserId, evaluatedUserId)
        .then((matched: boolean) => {
            if (matched) {
                return createDialog()
                    .then(newChatKey => sendWelcomeMessage(newChatKey))
                    .then(newChatKey => setMatchPersonEvaluations(newChatKey, evaluatingUserId, evaluatedUserId))
                    .then(newChatKey => setUniqueDialogId(newChatKey, evaluatingUserId, evaluatedUserId));
            } else {
                return
            }

        })
}

function handleMatch(personEvaluation: PersonEvaluation, evaluatingUserId: string, evaluatedUserId: string) {
    return setMatchedChatMember(personEvaluation.dialogId, evaluatingUserId)
        .then(() => updateFriendsCounter(true, evaluatingUserId))
        .then(() => sendMatchNotification(personEvaluation, evaluatedUserId, evaluatingUserId, evaluatedUserId));
}

function checkForMatch(evaluatingUserId: string, evaluatedUserId: string): Promise<boolean> {
    const likedUserEvaluationOfLikingUserPromise: Promise<DataSnapshot> = database
        .ref(`/evaluated_persons/${evaluatedUserId}/${evaluatingUserId}/status`)
        .once('value');

    return likedUserEvaluationOfLikingUserPromise
        .then((likedUserEvaluationOfLikingUser: DataSnapshot) => {
            if (likedUserEvaluationOfLikingUser.exists() &&
                (likedUserEvaluationOfLikingUser.val() === "LIKED" ||
                    likedUserEvaluationOfLikingUser.val() === "MATCHED")) {
                return Promise.resolve(true);
            } else {
                return Promise.reject(false);
            }
        })
}

function createDialog(): Promise<string> {
    let newDialogRef = database.ref('/dialogs').push();
    let newDialogId = newDialogRef.key;
    console.log('New dialog with id: ', newDialogId, " created");
    return newDialogRef.set({
        chatType: "DIALOG",
        dialogCreationTimestamp: admin.database.ServerValue.TIMESTAMP
    })
        .then(() => newDialogId)
}

function sendWelcomeMessage(chatId: string): Promise<string> {
    let message: Message = new Message("", null, new InitialMessageContent(), admin.database.ServerValue.TIMESTAMP);
    return database.ref("messages").child(chatId).push().set(message)
        .then(() => chatId)
}


function sendPersonRejoinedMessage(groupId: string, participantId: string): Promise<void> {
    return getUserName(participantId)
        .then((username: string) => new Message(participantId, username,
            new ParticipationPersonAddedContent(username, participantId), admin.database.ServerValue.TIMESTAMP))
        .then((message: Message) => database.ref("messages").child(groupId).push(message))
}

function sendPersonLeftMessage(groupId: string, participantId: string): Promise<void> {
    return getUserName(participantId)
        .then((username: string) => new Message(participantId, username,
            new ParticipationPersonLeftContent(username, participantId), admin.database.ServerValue.TIMESTAMP))
        .then((message: Message) => database.ref("messages").child(groupId).push(message))
}

function setMatchPersonEvaluations(chatId: string, evaluatingUserId: string, evaluatedUserId: string): Promise<string> {
    let updatedValues = {
        [evaluatingUserId + "/" + evaluatedUserId + "/status"]: "MATCHED",
        [evaluatedUserId + "/" + evaluatingUserId + "/status"]: "MATCHED"

    };
    return database.ref("evaluated_persons")
        .update(updatedValues)
        .then(() => chatId)
}

function setDislikedPersonEvaluations(evaluatingUserId: string, evaluatedUserId: string): Promise<void> {
    let updatedValues = {
        [evaluatingUserId + "/" + evaluatedUserId + "/status"]: "DISLIKED",
        [evaluatedUserId + "/" + evaluatingUserId + "/status"]: "DISLIKED"

    };
    return database.ref("evaluated_persons")
        .update(updatedValues)
}

function setMatchedChatMember(chatId: string, uId: string): Promise<string> {
    let chatMembership = new ChatVisibleChatMembership(uId, chatId, "DIALOG", "ACTIVE", null, null);
    return database.ref(Paths.MEMBERSHIPS_CHAT_AGGREGATED).child(chatId).child(uId).set(chatMembership).then(() => chatId);
}

function setLeftChatMember(chatId: string, uId: string): Promise<string> {
    let chatMembership = new ChatVisibleChatMembership(uId, chatId, "DIALOG", "EXPIRED", admin.database.ServerValue.TIMESTAMP, null);
    return database.ref(Paths.MEMBERSHIPS_CHAT_AGGREGATED).child(chatId).child(uId).set(chatMembership).then(() => chatId);
}


function sendMatchNotification(personEvaluation: PersonEvaluation, recepientId: string, evaluatingUserId: string, evaluatedUserId: string): Promise<void[]> {
    return Promise.all([getUserName(evaluatingUserId), getUserPhoto(evaluatingUserId),
        getUserName(evaluatedUserId), getUserPhoto(evaluatedUserId)])
        .then(values => {
            let evaluatingUserName = values[0];
            let evaluatingUserPhoto = values[1];
            let evaluatedUserName = values[2];
            let evaluatedUserPhoto = values[3];

            let matchNotification = new MatchPushNotification(personEvaluation.dialogId,
                evaluatedUserId, evaluatedUserName, evaluatedUserPhoto,
                evaluatingUserId, evaluatingUserName, evaluatingUserPhoto,
                personEvaluation.status, personEvaluation);

            const message = {
                data: {
                    match_data_key: JSON.stringify(matchNotification)
                }
            };
            return sendPushMessages([recepientId], message);
        })
}

function updateFriendsCounter(increment: boolean, userId: string): Promise<any> {
    return database.ref("users").child(userId).child("friends").transaction((currentCount: number) => {
        if (increment) {
            return (currentCount || 0) + 1
        } else {
            return (currentCount || 0) - 1
        }
    })
}

function setUniqueDialogId(chatId: string, evaluatingUserId: string, evaluatedUserId: string): Promise<any> {
    let transaction1 = database.ref("evaluated_persons")
        .child(evaluatingUserId)
        .child(evaluatedUserId)
        .child("dialogId")
        .transaction((existingDialogId: string) => {
            if (existingDialogId) {
                return existingDialogId
            } else {
                return chatId
            }
        });
    let transaction2 = database.ref("evaluated_persons")
        .child(evaluatedUserId)
        .child(evaluatingUserId)
        .child("dialogId")
        .transaction((existingDialogId: string) => {
            if (existingDialogId) {
                return existingDialogId
            } else {
                return chatId
            }
        });
    // There is still a small, small chance that users will end up in two different chats but it is better than
    // users creating two chats all the time. Perfect resolution would bo to store data in orderless hash node creted
    // from uIds of two users. But this is fine as well
    return Promise.all([transaction1, transaction2])
}

function externalUserEvaluationLike(evaluatingUId: string, evaluatedUId: string) {
    let requestOptions = {
        method: 'PUT',
        uri: getExternalEvaluationUri() + evaluatingUId + "/" + evaluatedUId,
        body: {
            status: "LIKED"
        },
        headers: {
            Authorization: getProjectMigrationAuthToken()
        },
        json: true
    };
    return request(requestOptions)
        .catch(error => {
            console.log("Failed to send evaluation to external service: " + error.toString());
            return
        })
}

function externalUserEvaluationDislike(evaluatingUId: string, evaluatedUId: string) {
    let requestOptions = {
        method: 'PUT',
        uri: getExternalEvaluationUri() + evaluatingUId + "/" + evaluatedUId,
        body: {
            status: "DISLIKED"
        },
        headers: {
            Authorization: getProjectMigrationAuthToken()
        },
        json: true
    };
    return request(requestOptions)
        .catch(error => {
            console.log("Failed to send evaluation to external service: " + error.toString());
            return
        })
}


class MatchPushNotification {
    constructor(readonly dialogId: string,
                readonly likedUserId: string,
                readonly likedUserName: string,
                readonly likedUserPhoto: string,
                readonly likingUserId: string,
                readonly likingUserName: string,
                readonly likingUserPhoto: string,
                readonly evaluationStatus: PersonEvaluationStatus,
                //Deprecated values
                readonly personEvaluation: PersonEvaluation) {
    }
}
