/**
 * Created by maciek on 06.04.17.
 */
import * as functions from 'firebase-functions';
import * as admin from "firebase-admin";

const GeoFire = require('geofire');
const request = require('request-promise-native');

import {
    Location, ExternalLocation
} from './model'
import {getExternalLocationUri} from './uriProvider';
import {getProjectMigrationAuthToken} from "./credentialsProvider";
import {EventContext} from "firebase-functions";
import {Change} from "firebase-functions";

type DataSnapshot = admin.database.DataSnapshot;
const database: admin.database.Database = admin.database();

export let handleUserLocation = functions.database.ref('/user_locations/{userId}')
    .onWrite((change, context) => handleUserLocationChanged(change, context));


function handleUserLocationChanged(change: Change<DataSnapshot>, context: EventContext): Promise<void[]> {
    const userId: string = context.params.userId;

    let previousDataSnapshot: DataSnapshot = change.before;
    let currentDataSnapshot: DataSnapshot = change.after;
    let previousData: Location = previousDataSnapshot.val();
    let currentData: Location = currentDataSnapshot.val();
    console.log('previousData: ', previousData, ', currentData:', currentData);

    if (!currentDataSnapshot.exists()) {
        let removeGeolocation = removeUserGeolocation(userId);
        let removeExternalLocation = externalUserLocationRemove(userId);
        return Promise.all([removeGeolocation, removeExternalLocation])
    } else {
        let insertGeolocation = insertUserGeolocation(userId, currentData);
        let insertExternalLocation = externalUserLocationInsert(userId, currentData);
        return Promise.all([insertGeolocation, insertExternalLocation])

    }
}

function insertUserGeolocation(userId: string, currentData: Location) {
    return new GeoFire(database.ref("geomatching").child("user_locations"))
        .set(userId, [currentData.latitude, currentData.longitude]);
}

function removeUserGeolocation(userId: string) {
    return new GeoFire(database.ref("geomatching")
        .child("user_locations")).remove(userId);
}

function externalUserLocationInsert(userId: string, currentData: Location) {
    let externalLocation = new ExternalLocation(currentData);
    let requestOptions = {
        method: 'PUT',
        uri: getExternalLocationUri() + userId,
        qs: userId,
        body: externalLocation,
        headers: {
            Authorization: getProjectMigrationAuthToken()
        },
        json: true
    };
    return request(requestOptions)
        .catch(error => {
            console.log("Failed to send location to external service: " + error.toString());
            return
        })
}

function externalUserLocationRemove(userId: string) {
    let requestOptions = {
        method: 'DELETE',
        uri: getExternalLocationUri() + userId,
        qs: userId,
        headers: {
            Authorization: getProjectMigrationAuthToken()
        },
        json: true
    };
    return request(requestOptions)
        .catch(error => {
            console.log("Failed to send location to external service: " + error.toString());
            return
        })
}