/**
 * Created by maciek on 05.04.17.
 */

export class Paths {
    public static readonly MEMBERSHIPS_USER_AGGREGATED: string = "memberships_user_aggregated";
    public static readonly MEMBERSHIPS_CHAT_AGGREGATED: string = "memberships_chat_aggregated";
    public static readonly GROUPS: string = "groups";
    public static readonly GROUP_JOIN_REQUESTS: string = "group_join_requests";
    public static readonly GROUP_JOIN_REQUESTS_USER_AGGREGATED: string = "group_join_requests_user_aggregated";
    public static readonly INITIAL_USER_CREDENTALS: string = "initial_user_credentials";
    public static readonly GDPR_CONSENTS: string = "gdpr_consents";
    public static readonly DELETED_USERS: string = "deleted_users";

    public static readonly GEOMATCHING: string = "geomatching";
    public static readonly GEOMATCHING_GROUPS: string = "group_locations";

}

export class Key<T> {
    constructor(readonly key: string,
                readonly val: T) {

    }
}

export class KeyedGroup {
    constructor(readonly key: string,
                readonly value: Group) {
    }
}


export class User {
    constructor(readonly displayName: string,
                readonly fullName: string,
                readonly about: string,
                readonly gender: Gender,
                readonly age: number) {
    }
}

export class InitialUserCredentials {
    constructor(readonly providerType: string,
                readonly token: string) {
    }
}


export type Gender = "MALE" | "FEMALE"

export class Photo {
    constructor(readonly uri: string,
                readonly uploadStatus: UploadStatus,
                readonly index: number) {
    }
}

export type UploadStatus = "FINISHED" | "PENDING" | "NOT_STARTED" | "LOCAL";

export class ChatVisibleChatMembership {
    constructor(readonly uId: string,
                readonly chatId: string,
                readonly chatType: ChatType,
                readonly membershipStatus: MembershipStatus,
                readonly leaveTimestamp: any,
                readonly lastInteraction: any) {
    }
}

export class UserVisibleChatMembership {
    constructor(readonly uId: string,
                readonly chatId: string,
                readonly chatType: ChatType,
                readonly membershipStatus: MembershipStatus,
                readonly leaveTimestamp: any,
                readonly lastInteraction: any) {
    }
}

export type ChatType = "DIALOG" | "GROUP"

export type MembershipStatus = "ACTIVE" | "EXPIRED"

export class Group {
    constructor(readonly name: string,
                readonly description: string,
                readonly image: string,
                readonly location: Place,
                readonly groupCreationTimestamp: any,
                readonly meetingTime: any,
                readonly groupOwnerId: string,
                readonly lastEditorId: string,
                readonly membersCount: number,
                //Below arguments are for server use only, won't be settable or needed on client side
                readonly discoverable: boolean,
                readonly activated: boolean,
                readonly meetingReminderSent: boolean,
                readonly meetingNoticeSent: boolean,
                readonly deactivated: boolean) {
    }
}

export type GroupMeetingNotificationType = "REMINDER" | "NOTICE" | "DEACTIVATION"

export type GroupParticipationChange = "ADDED" | "REMOVED" | "LEFT"

export class Message {
    constructor(readonly userId: string,
                readonly userName: string,
                readonly content: MessageContent,
                readonly timestamp: any) {
    }
}

export type ContentType = "INITIAL_MESSAGE" |
    "TEXT" |
    "IMAGE" |
    "REMOVED" |
    "PARTICIPATION_PERSON_ADDED" |
    "PARTICIPATION_PERSON_REMOVED" |
    "PARTICIPATION_PERSON_LEFT" |
    "PARTICIPATION_PERSON_DELETED_ACCOUNT" |
    "GROUP_TIME_SET" |
    "GROUP_LOCATION_SET" |
    "GROUP_NAME_SET" |
    "GROUP_DESCRIPTION_SET" |
    "GROUP_IMAGE_SET" |
    "UNKNOWN"

export abstract class MessageContent {
    constructor(readonly contentType: ContentType) {
    }
}

export class GroupDescriptionSetContent extends MessageContent {
    constructor(readonly newGroupDescription: string) {
        super("GROUP_DESCRIPTION_SET");
    }
}

export class GroupImageSetContent extends MessageContent {
    constructor(readonly newImageUrl: string) {
        super("GROUP_IMAGE_SET");
    }
}

export class GroupLocationSetContent extends MessageContent {
    constructor(readonly meetingPlace: Place,
                readonly initial: boolean) {
        super("GROUP_LOCATION_SET");
    }
}

export class GroupNameSetContent extends MessageContent {
    constructor(readonly newGroupName: string) {
        super("GROUP_NAME_SET");
    }
}

export class GroupTimeSetContent extends MessageContent {
    constructor(readonly timestamp: number,
                readonly initial: boolean) {
        super("GROUP_TIME_SET");
    }
}

export class ParticipationPersonAddedContent extends MessageContent {
    constructor(readonly addedPersonName: string,
                readonly addedPersonId: string) {
        super("PARTICIPATION_PERSON_ADDED");
    }
}

export class ParticipationPersonLeftContent extends MessageContent {
    constructor(readonly personLeftName: string,
                readonly personLeftId: string) {
        super("PARTICIPATION_PERSON_LEFT");
    }
}

export class ParticipationPersonDeletedAccountContent extends MessageContent {
    constructor(readonly personDeletedAccountName: string,
                readonly personDeletedAccountId: string) {
        super("PARTICIPATION_PERSON_DELETED_ACCOUNT");
    }
}

export class ParticipationPersonRemovedContent extends MessageContent {
    constructor(readonly removedPersonName: string,
                readonly removedPersonId: string) {
        super("PARTICIPATION_PERSON_REMOVED");
    }
}

export class TextContent extends MessageContent {
    constructor(readonly text: string) {
        super("TEXT");
    }
}

export class ImageContent extends MessageContent {
    constructor(readonly uri: string,
                readonly dimensionRatio: number,
                readonly uploadStatus: UploadStatus) {
        super("TEXT");
    }
}

export class InitialMessageContent extends MessageContent {
    constructor() {
        super("INITIAL_MESSAGE");
    }
}

export class RemovedContent extends MessageContent {
    constructor() {
        super("REMOVED");
    }
}


export class UnknownContent extends MessageContent {
    constructor() {
        super("UNKNOWN");
    }
}

export class Place {
    constructor(readonly name: string,
                readonly address: string,
                readonly latitude: number,
                readonly longitude: number) {
    }
}

export class Location {
    constructor(readonly locationName: string,
                readonly personUid: string,
                readonly type: LocationType,
                readonly range: number,
                readonly latitude: number,
                readonly longitude: number) {
    }
}

export class ExternalLocation {
    locationName: string;
    type: LocationType;
    range: number;
    latitude: number;
    longitude: number;
    constructor(location: Location){
        this.locationName=location.locationName;
        this.type=location.type;
        this.range=location.range;
        this.latitude=location.latitude;
        this.longitude=location.longitude;
    }
}

export type LocationType = "CUSTOM_LOCATION" | "CURRENT_LOCATION";

export class GroupJoinRequest {
    constructor(readonly status: GroupJoinRequestStatus,
                readonly lastEditorId: string) {
    }
}

export type GroupJoinRequestStatus =
    "NONE"
    | "INITIAL"
    | "NOT_INTERESTED"
    | "MEMBERSHIP_REQUESTED"
    | "ACCEPTED"
    | "REJECTED"
    | "REMOVED"
    | "LEFT"

export class PersonEvaluation {
    constructor(readonly status: PersonEvaluationStatus,
                readonly dialogId: string) {
    }
}

export type PersonEvaluationStatus = "LIKED" | "DISLIKED" | "MATCHED"

export type GdprConsent = "NONE" | "REFUSED" | "AGREED_V1"