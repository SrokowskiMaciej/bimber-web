import {getProjectId} from './credentialsProvider';

function getProjectUri() {
    return "https://" + getProjectId() + ".appspot.com/";
}

export function getExternalLocationUri() {
    return getProjectUri().concat("migration/usersLocations/")
}

export function getExternalEvaluationUri() {
    return getProjectUri().concat("migration/evaluation/")
}