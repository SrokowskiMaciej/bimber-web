/**
 * Created by maciek on 06.04.17.
 */
import * as admin from "firebase-admin";

type DataSnapshot = admin.database.DataSnapshot;

export function sendPushMessages(usersIds: string[], message): Promise<void[]> {
    const messagingTokensRef: admin.database.Reference = admin.database().ref('messaging_tokens');
    const usersTokens: Promise<DataSnapshot>[] = usersIds.map(uId => messagingTokensRef.child(uId).once("value"));

    return Promise.all(usersTokens)
        .then((tokensSnapshots: DataSnapshot[]) => {
            let uidTokenMap: string[][] = tokensSnapshots.map((dataSnapshot: DataSnapshot) => {
                if (dataSnapshot.hasChildren()) {
                    let key: string = dataSnapshot.ref.key;
                    let tokens: string[] = Object.keys(dataSnapshot.val());
                    return tokens.map((token: string) => [key, token])
                } else {
                    return [];
                }
            }).reduce((acc: string[][], cur: string[][]) => acc.concat(cur), []);

            console.log("Sending notifications to users with tokens: ", JSON.stringify(uidTokenMap));
            const tokens: string[] = uidTokenMap.map(tokenRef => tokenRef[1]);
            console.log("Message: ", JSON.stringify(message));

            let options = {
                priority: "high",
            };

            return admin.messaging().sendToDevice(tokens, message, options)
                .then((response: admin.messaging.MessagingDevicesResponse) => {
                    // For each message check if there was an error.
                    const tokensToRemove: Promise<void>[] = [];
                    response.results.forEach((result, index) => {
                        const error: admin.FirebaseError = result.error;
                        let uId: string = uidTokenMap[index][0];
                        let token: string = uidTokenMap[index][1];
                        if (error) {
                            console.log('Failure sending notification to user: ', uId, ", token: ", token, error);
                            // Cleanup the tokens that are not registered anymore.
                            if (error.code === 'messaging/invalid-registration-token' ||
                                error.code === 'messaging/registration-token-not-registered') {
                                console.log("Removing token");
                                tokensToRemove.push(messagingTokensRef
                                    .child(uId)
                                    .child(token)
                                    .remove());
                            } else {
                                console.error("NON HANDLED ERROR");
                            }
                        } else {
                            console.log('Sent notification to user: ', uId, ", token: ", token, ", result: ", result);
                        }
                    });
                    return Promise.all(tokensToRemove);
                });
        });
}
