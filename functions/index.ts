'use strict';

const functions = require('firebase-functions');
const admin = require('firebase-admin');

admin.initializeApp();

export * from './handleUserEvaluation';
export * from './handleNewMessage';
export * from './onChatMembership'
export * from './handleGroupParticipants'
export * from './handleGroupChanges'
export * from './handleUserLocation'
export * from './handleNewUser'
export * from './handleDeletedUser'
export * from './subscribeToPeriodicGroupTimeCheckTick'
export * from './onInitialUserCredentials'
export * from './onStoragePhotoUploaded'
export * from './handleGdprConsent'